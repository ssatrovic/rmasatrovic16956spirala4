package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class Pocetni extends ActionBarActivity implements FragmentListaGlumaca.onItemClick {

    private AdapterGlumac glumacAdapter;
    private ArrayList<Glumac> glumci;
    private boolean siriL;
    public static int odabraniGlumacPozicija=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_pocetni);
        glumci=GlumacPomocna.getInstance().getGlumci();
        Baza b=new Baza(this);

        /*if(glumci.size()==0)
        {
            Glumac g=new Glumac("bot","bot","bot","bot","bot","bot","bot","bot");
            g.setgTheMoviedbID("1");
            glumci.add(g);
        }*/
        /*siriL = false;

        FragmentManager fm = getFragmentManager();
        FrameLayout fragmentDetalji = (FrameLayout)findViewById(R.id.mjestoOstalo);

        if(fragmentDetalji != null) //za široke ekrane
        {
            siriL = true;

            FragmentDvaDugmeta fdva=new FragmentDvaDugmeta();
            fm.beginTransaction().replace(R.id.mjestoButtoni,fdva).commit();

            Fragment fd;
            fd = fm.findFragmentById(R.id.mjestoOstalo);

            if(fd == null)
            {
                fd = new FragmentDetaljiGlumac();

                Bundle arguments = new Bundle();
                arguments.putInt("Izabrani", 0);
                fd.setArguments(arguments);
                fm.beginTransaction().replace(R.id.mjestoOstalo, fd, "Detalji").commit();
            }

        }
        else
        {
            FragmentTriDugmeta ftri=new FragmentTriDugmeta();
            fm.beginTransaction().replace(R.id.mjestoButtoni,ftri).commit();
        }

        FragmentListaGlumaca fl = (FragmentListaGlumaca) fm.findFragmentByTag("Lista");

        if(fl == null)
        {
            fl = new FragmentListaGlumaca();

            fm.beginTransaction().replace(R.id.mjestoGlumci, fl, "Lista").commit();
        }
        else
        {

            fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }


*/

        siriL=false;

        FragmentManager fm = getFragmentManager();
        FrameLayout fragmentDetalji = (FrameLayout)findViewById(R.id.mjestoOstalo);

        if(fragmentDetalji!=null)
        {
            siriL=true;
            FragmentDvaDugmeta fdva=new FragmentDvaDugmeta();
            fm.beginTransaction().replace(R.id.mjestoButtoni,fdva).commit();

            Bundle argumenti=new Bundle();
            //argumenti.putParcelable("glumac",glumci.get(0));
            argumenti.putInt("Izabrani", 0);
            FragmentDetaljiGlumac fd=new FragmentDetaljiGlumac();
            fd.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoOstalo,fd).commit();
            fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        else
        {
            FragmentTriDugmeta ftri=new FragmentTriDugmeta();
            fm.beginTransaction().replace(R.id.mjestoButtoni,ftri).commit();
        }

        FragmentListaGlumaca fl=new FragmentListaGlumaca();
        Bundle argumenti =new Bundle();
        argumenti.putParcelableArrayList("Alista",glumci);

        fl.setArguments(argumenti);

        fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").commit();


    }

    @Override
    public void onItemClicked(int pos) {
        odabraniGlumacPozicija=pos;

        Bundle arguments = new Bundle();
        arguments.putInt("Izabrani", pos);
        FragmentDetaljiGlumac fd = new FragmentDetaljiGlumac();

        fd.setArguments(arguments);
        if(siriL)
        {

            getFragmentManager().beginTransaction().replace(R.id.mjestoOstalo, fd).commit();
        }
        else
        {

            getFragmentManager().beginTransaction().replace(R.id.mjestoGlumci, fd).addToBackStack(null).commit();
        }
    }

    @Override
    public void onBackPressed()
    {
        if(getFragmentManager().getBackStackEntryCount() != 0)
        {
            getFragmentManager().popBackStack();
        }
        else
        {
            super.onBackPressed();
        }
    }

}
