package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Salva on 18-May-17.
 */

public class SkiniPodatkeGlumca {

    public static void  DajSliku(String glumacID, String imgUrl)
    {
        String query = imgUrl;

        try {
            URL url = new URL(query);

            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n=in.read(buf)))
            {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            Bitmap bitmap = BitmapFactory.decodeByteArray(response, 0, response.length);

            String glumacTheMoviedb = glumacID;
            ArrayList<Glumac> listaGlumaca = GlumacPomocna.getInstance().getGlumci();
            for(int i = 0; i < listaGlumaca.size(); i++)
            {
                if(listaGlumaca.get(i).getgTheMoviedbID() == glumacTheMoviedb) {
                    listaGlumaca.get(i).setSlikaGlumca(bitmap);
                    break;
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //nabitnija
    public static void DajGlumca(String unos) {
        String query = null;
        String query2 = null;
        try {
            query = URLEncoder.encode(unos, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://api.themoviedb.org/3/search/person?api_key=85612f42fdfb48bffcf1478271feb3fe&query=" + query;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray rezultati = jo.getJSONArray("results");

           int duzina=5;
            if(rezultat.length()<5)
                duzina=rezultati.length();
            GlumacPomocna.getInstance().getGlumci().clear();
            for (int i = 0; i < duzina; i++)
            {
                JSONObject glumac = rezultati.getJSONObject(i);
                final String glumac_ID = glumac.getString("id");

                try {
                    query2 = URLEncoder.encode(glumac_ID, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
/////////////////////////////////////



                String url2 = "https://api.themoviedb.org/3/person/"+query2+"?api_key=85612f42fdfb48bffcf1478271feb3fe";
                try
                {
                    URL urldetalji = new URL(url2);
                    HttpURLConnection urlConnection2 = (HttpURLConnection) urldetalji.openConnection();
                    InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());
                    String rezultat2 = convertStreamToString(in2);
                    JSONObject jo2 = new JSONObject(rezultat2);

                    String ime=jo2.getString("name");
                    String godrodj=jo2.getString("birthday");
                    String godsmr=jo2.getString("deathday");
                    String mjestorodj=jo2.getString("place_of_birth");
                    String rating=jo2.getString("popularity");
                    Double tmp= Double.parseDouble(rating);
                    tmp=round(tmp,2);
                    rating=tmp.toString();
                    String bio=jo2.getString("biography");
                    String imdbid=jo2.getString("imdb_id");
                    String link="www.imdb.com/name/"+imdbid+"/";
                    String spolId=jo2.getString("gender");
                    String spol;
                    if(spolId=="1") spol="zenski";
                    else spol="muski";


                    String urlSlika = "https://api.themoviedb.org/3/person/"+query2+"/images?api_key=85612f42fdfb48bffcf1478271feb3fe";

                     //slike=new JSONArray();
                    try {
                        URL urlS = new URL(urlSlika);
                        HttpURLConnection urlConnectionS = (HttpURLConnection) urlS.openConnection();
                        InputStream inS = new BufferedInputStream(urlConnectionS.getInputStream());
                        String rezultatS = convertStreamToString(inS);
                        JSONObject joS = new JSONObject(rezultatS);
                        JSONArray slike = joS.getJSONArray("profiles");

                        JSONObject imag = new JSONObject();
                        try {
                            imag = slike.getJSONObject(0);
                            String komadLinka=imag.getString("file_path");
                            final String imgUrl = ("http://image.tmdb.org/t/p/w185//"+komadLinka);

                            if(imgUrl != null) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        SkiniPodatkeGlumca.DajSliku(glumac_ID, imgUrl);
                                    }
                                }).start();
                            }

                        }catch (Exception c) { Log.i("ERROR", c.getMessage()); }

                    }
                    catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                    /*try
                    {
                        new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                SkiniPodatkeGlumca.DajZanroveZaGlumca(glumac_ID);
                            }
                        }).start();
                    }catch (Exception c) { Log.i("ERROR", c.getMessage()); }
                    try
                    {
                        new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                SkiniPodatkeGlumca.DajRezisereZaGlumca(glumac_ID);
                            }
                        }).start();
                    }catch (Exception c) { Log.i("ERROR", c.getMessage()); }*/


                    Glumac rez = new Glumac(ime,godrodj,godsmr,mjestorodj,rating,bio,link,spol);
                    rez.setgTheMoviedbID((glumac_ID).toString());

                   //GlumacPomocna.getInstance().getGlumci().add(i,rez);
                    FragmentListaGlumaca.glumacAdapter.addGlumac(rez);

                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //pomocne
    public static void DajZanroveZaGlumca(String glumacId)
    {
        ArrayList<Zanr> listaZanrova=new ArrayList<Zanr>();
        String query="https://api.themoviedb.org/3/person/"+glumacId+"/movie_credits?api_key=85612f42fdfb48bffcf1478271feb3fe&language=en-US";
        String query2=null;
        try{
            URL url = new URL(query);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray rezultati = jo.getJSONArray("cast");


            Date najveci=new Date();
            JSONObject trenutniFilm=new JSONObject();

            int brfilmova=7;
            if(rezultati.length()<7)
                brfilmova=rezultati.length();
            for(int i=0;i<brfilmova;i++)
            {
                Date trenutni=null;
                for(int j=0;j<rezultati.length();j++)
                {
                    jo = rezultati.getJSONObject(j);
                    Date pomocni = dateOrNull(jo, "release_date");
                    if(pomocni != null && pomocni.getTime() < najveci.getTime() && (trenutni == null || dateOrNull(jo, "release_date").getTime() > trenutni.getTime())){
                        trenutniFilm = jo;
                        trenutni = pomocni;
                    }
                }

                //JSONObject film = rezultati.getJSONObject(i);

                final String filmId=trenutniFilm.getString("id");
                try {
                    query2 = URLEncoder.encode(filmId, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                String url2 = "https://api.themoviedb.org/3/movie/"+query2+"?api_key=85612f42fdfb48bffcf1478271feb3fe&language=en-US";

                try
                {
                    URL urldetalji = new URL(url2);
                    HttpURLConnection urlConnection2 = (HttpURLConnection) urldetalji.openConnection();
                    InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());
                    String rezultat2 = convertStreamToString(in2);
                    JSONObject jo2 = new JSONObject(rezultat2);
                    JSONArray nizZanrova= jo2.getJSONArray("genres");

                    if(nizZanrova.length()!=0)
                    {
                        boolean ima=false;
                        JSONObject zanr1 = nizZanrova.getJSONObject(0);
                        String imeZanra=zanr1.getString("name");

                        for(int k=0;k<listaZanrova.size();k++)
                        {
                            if(listaZanrova.get(k).getIme().equals(imeZanra))
                            {
                                ima=true;
                                break;
                            }
                        }
                        if(ima==false)
                        {
                            Zanr taj=new Zanr(imeZanra);
                            listaZanrova.add(taj);
                        }

                    }


                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                najveci = trenutni;

            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FragmentDetaljiGlumac.listaZanrova=listaZanrova;


    }

    public static void DajRezisereZaGlumca(String glumacId)
    {
        ArrayList<Reziser> listaRezisera=new ArrayList<Reziser>();

        String query="https://api.themoviedb.org/3/person/"+glumacId+"/movie_credits?api_key=85612f42fdfb48bffcf1478271feb3fe&language=en-US";
        String query2=null;
        try{
            URL url = new URL(query);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray rezultati = jo.getJSONArray("cast");

            Date najveci=new Date();
            JSONObject trenutniFilm=new JSONObject();

int brfilmova=7;
            if(rezultat.length()<7)
                brfilmova=rezultat.length();
            for(int i=0;i<brfilmova;i++)
            {

                Date trenutni=null;
                for(int j=0;j<rezultati.length();j++)
                {
                    jo = rezultati.getJSONObject(j);
                    Date privremeni = dateOrNull(jo, "release_date");
                    if(privremeni != null && privremeni.getTime() < najveci.getTime() && (trenutni == null || dateOrNull(jo, "release_date").getTime() > trenutni.getTime())){
                        trenutniFilm = jo;
                        trenutni = privremeni;
                    }
                }

                //JSONObject film = rezultati.getJSONObject(i);
                final String filmId=trenutniFilm.getString("id");
                try {
                    query2 = URLEncoder.encode(filmId, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                String url2 = "https://api.themoviedb.org/3/movie/"+query2+"/credits?api_key=85612f42fdfb48bffcf1478271feb3fe";

                try
                {
                    URL urldetalji = new URL(url2);
                    HttpURLConnection urlConnection2 = (HttpURLConnection) urldetalji.openConnection();
                    InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());
                    String rezultat2 = convertStreamToString(in2);
                    JSONObject jo2 = new JSONObject(rezultat2);
                    JSONArray nizCrew = jo2.getJSONArray("crew");

                    for(int j=0;j<nizCrew.length();j++)
                    {
                        boolean ima=false;
                        JSONObject crew = nizCrew.getJSONObject(j);
                        String imeRezisera=crew.getString("name");
                        String jobRezisera=crew.getString("job");
                        String idRezisera=crew.getString("id");
                        if(jobRezisera.toLowerCase().equals("director"))
                        {

                            for(int k=0;k<listaRezisera.size();k++)
                            {
                                if(listaRezisera.get(k).getIme().equals(imeRezisera))
                                {
                                    ima=true;
                                    break;
                                }
                            }
                            if(ima==false)
                            {
                                Reziser taj=new Reziser(imeRezisera,idRezisera);
                                listaRezisera.add(taj);
                            }


                        }
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                najveci = trenutni;
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FragmentDetaljiGlumac.listaRezisera=listaRezisera;
    }


    public static Date dateOrNull(JSONObject object, String key){
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return format.parse(object.getString(key));
        } catch (Exception ex) {
            return null;
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }



    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        } catch (IOException e)
        {
        } finally
        {
            try {
                is.close();
            } catch (IOException e)
            {
            }
        }
        return sb.toString();
    }

}
