package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Salva on 11-Jun-17.
 */

public class Baza extends SQLiteOpenHelper{

    public static final String ImeBaze="rmaGlumci.db";
    public static final int verzija_baze=34;
    SQLiteDatabase db = getWritableDatabase();

    //konstruktori
    public Baza(Context context, String name, SQLiteDatabase.CursorFactory factory,int version)
    {
        super(context,name,factory,version);
    }

    public Baza (Context context)
    {

        super(context,ImeBaze,null,verzija_baze);
        //SQLiteDatabase db=getWritableDatabase();
    }

    ///////////////////////7 GLUMCI TABELA
        public static final String Glumac_tabela = "glumci";
        public static final String Glumac_gTheMoviedbID = "tmdbid";
        public static final String Glumac_ime = "ime";
        public static final String Glumac_godinaRodjenja = "godinaRodjenja";
        public static final String Glumac_godinaSmrti = "godinaSmrti";
        public static final String Glumac_mjestoRodjenja = "mjestoRodjenja";
        public static final String Glumac_rating = "rating";
        public static final String Glumac_biografija = "biografija";
        public static final String Glumac_link = "link";
        public static final String Glumac_spol = "spol";
        public static final String Glumac_slikaGlumca = "slika";


        private static final String glumciTabelaCreate =
                "CREATE TABLE " + Glumac_tabela + " ("
                        + Glumac_gTheMoviedbID + " TEXT PRIMARY KEY, "
                        + Glumac_ime + " TEXT NOT NULL, "
                        + Glumac_godinaRodjenja + " TEXT NOT NULL, "
                        + Glumac_godinaSmrti + " TEXT NOT NULL, "
                        + Glumac_mjestoRodjenja + " TEXT NOT NULL, "
                        + Glumac_rating + " TEXT NOT NULL, "
                        + Glumac_biografija + " TEXT NOT NULL, "
                        + Glumac_link + " TEXT NOT NULL, "
                        + Glumac_spol + " TEXT NOT NULL, "
                        + Glumac_slikaGlumca + " BLOB);";

        private static final String DropajGlumce = "DROP TABLE IF EXISTS " + Glumac_tabela;

///////////////////// TABELA REZISER
    public static final String Reziser_tabela="reziseri";
    public static final String Reziser_id="id";
    public static final String Reziser_ime="imeReziser";


    private static final String reziserTabelaCreate=
            "CREATE TABLE "+ Reziser_tabela+" ("
            +Reziser_id+" TEXT PRIMARY KEY, "
            +Reziser_ime+" TEXT NOT NULL);";

    private static final String DropajRezisere = "DROP TABLE IF EXISTS " + Reziser_tabela;


    ///////////////////TABELA ZANROVI
    public static final String Zanr_tabela="zanrovi";
    public static final String Zanr_ime="ime";


    private static final String zanrTabelaCreate=
            "CREATE TABLE "+ Zanr_tabela+" ("
                    +Zanr_ime+" TEXT PRIMARY KEY);";

    private static final String DropajZanrove = "DROP TABLE IF EXISTS " + Zanr_tabela;

    ///////////////////// TABELA GLUMACZANR
    public static final String GlumacZanr_tabela="glumciZanrovi";
    public static final String GZanr_IME="zanr_ime";
    public static final String ZGlumac_id="glumac_id";


    private static final String glumacZanrTabelaCreate=
            "CREATE TABLE "+ GlumacZanr_tabela+" ("
                    +GZanr_IME+" INTEGER NOT NULL, "
                    +ZGlumac_id+" TEXT NOT NULL, "
                    +"FOREIGN KEY ("+ZGlumac_id+") REFERENCES "+ Glumac_tabela+"("+Glumac_gTheMoviedbID+"), "
                    +"FOREIGN KEY ("+GZanr_IME+") REFERENCES "+ Zanr_tabela+"("+Zanr_ime+"));";

    private static final String DropajGlumacZanrove = "DROP TABLE IF EXISTS " + GlumacZanr_tabela;

    ///////////////////// TABELA GLUMACREZISER
    public static final String GlumacReziser_tabela="glumciReziseri";
    public static final String GReziser_id="reziser_id";
    public static final String RGlumac_id="glumac_id";


    private static final String glumacReziserTabelaCreate=
            "CREATE TABLE "+ GlumacReziser_tabela+" ("
                    +GReziser_id+" INTEGER NOT NULL, "
                    +RGlumac_id+" TEXT NOT NULL, "
                    +"FOREIGN KEY ("+RGlumac_id+") REFERENCES "+ Glumac_tabela+"("+Glumac_gTheMoviedbID+"), "
                    +"FOREIGN KEY ("+GReziser_id+") REFERENCES "+ Reziser_tabela+"("+Reziser_id+"));";

    private static final String DropajGlumacRezisere = "DROP TABLE IF EXISTS " + GlumacReziser_tabela;

    @Override
    public void onCreate(SQLiteDatabase db) {

        //String kod="CREATE TABLE Persons (PersonID TEXT,LastName TEXT,FirstName TEXT,Address TEXT,City TEXT);";

       // db.execSQL(kod);
        db.execSQL(glumciTabelaCreate);
        db.execSQL(zanrTabelaCreate);
        db.execSQL(reziserTabelaCreate);
        db.execSQL(glumacZanrTabelaCreate);
        db.execSQL(glumacReziserTabelaCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DropajGlumce);
        db.execSQL(DropajZanrove);
        db.execSQL(DropajRezisere);
        db.execSQL(DropajGlumacZanrove);
        db.execSQL(DropajGlumacRezisere);
        onCreate(db);
    }


    public  void DodajGlumca(Glumac g)
    {
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        g.getSlikaGlumca().compress(Bitmap.CompressFormat.PNG,0,stream);
        byte[] slika=stream.toByteArray();
        ContentValues cv=new ContentValues();
        cv.put(Glumac_gTheMoviedbID,g.getgTheMoviedbID());
        cv.put(Glumac_ime,g.getIme());
        cv.put(Glumac_godinaRodjenja,g.getGodinaRodjenja());
        cv.put(Glumac_godinaSmrti,g.getGodinaSmrti());
        cv.put(Glumac_mjestoRodjenja,g.getMjestoRodjenja());
        cv.put(Glumac_rating,g.getRating());
        cv.put(Glumac_biografija,g.getBiografija());
        cv.put(Glumac_link,g.getLink());
        cv.put(Glumac_spol,g.getSpol());
        cv.put(Glumac_slikaGlumca,slika);

        db.insert(Glumac_tabela,null,cv);

        ContentValues cvZanrovi=new ContentValues();
        for(int i=0;i<FragmentDetaljiGlumac.listaZanrova.size();i++)
        {
            cvZanrovi.put(Zanr_ime,FragmentDetaljiGlumac.listaZanrova.get(i).getIme());
            db.insert(Zanr_tabela,null,cvZanrovi);

            ContentValues tmp2=new ContentValues();
            tmp2.put(GZanr_IME,FragmentDetaljiGlumac.listaZanrova.get(i).getIme());
            tmp2.put(ZGlumac_id,g.getgTheMoviedbID());
            db.insert(GlumacZanr_tabela,null,tmp2);

        }

        ContentValues cvReziseri=new ContentValues();
        for(int i=0;i<FragmentDetaljiGlumac.listaRezisera.size();i++)
        {
            cvReziseri.put(Reziser_id,FragmentDetaljiGlumac.listaRezisera.get(i).getId());
            cvReziseri.put(Reziser_ime,FragmentDetaljiGlumac.listaRezisera.get(i).getIme());
            db.insert(Reziser_tabela,null,cvReziseri);

            ContentValues tmp=new ContentValues();
            tmp.put(GReziser_id,FragmentDetaljiGlumac.listaRezisera.get(i).getId());
            tmp.put(RGlumac_id,g.getgTheMoviedbID());
            db.insert(GlumacReziser_tabela,null,tmp);
        }




    }

    public void DajGlumce(String unos)
    {
        Cursor cursor=db.rawQuery("SELECT * FROM "+Glumac_tabela,null);

        if(cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                String imeGlumca=cursor.getString(cursor.getColumnIndex("ime"));
                if(imeGlumca.toLowerCase().contains(unos))
                {
                    String tmdbid=cursor.getString(cursor.getColumnIndex("tmdbid"));
                    String godinarodj=cursor.getString(cursor.getColumnIndex("godinaRodjenja"));
                    String godinasmrti=cursor.getString(cursor.getColumnIndex("godinaSmrti"));
                    String mjestorodj=cursor.getString(cursor.getColumnIndex("mjestoRodjenja"));
                    String rating=cursor.getString(cursor.getColumnIndex("rating"));
                    String bio=cursor.getString(cursor.getColumnIndex("biografija"));
                    String link=cursor.getString(cursor.getColumnIndex("link"));
                    String spol=cursor.getString(cursor.getColumnIndex("spol"));
                    byte[] slika=cursor.getBlob(cursor.getColumnIndex("slika"));
                    Bitmap sl= BitmapFactory.decodeByteArray(slika,0,slika.length);

                    Glumac gTmp=new Glumac(imeGlumca,godinarodj,godinasmrti,mjestorodj,rating,bio,link,spol);
                    gTmp.setSlikaGlumca(sl);
                    gTmp.setgTheMoviedbID(tmdbid);

                    FragmentListaGlumaca.glumacAdapter.addGlumac(gTmp);

                }
                cursor.moveToNext();
            }
        }
    }

    public void postaviZanroveRezisere(String idGlumca)
    {
        ArrayList<Zanr> listaZanrova=new ArrayList<Zanr>();
        ArrayList<Reziser> listaRezisera=new ArrayList<Reziser>();
        Cursor cursor=db.rawQuery("SELECT * FROM "+GlumacZanr_tabela+" WHERE glumac_id="+idGlumca,null);

        if(cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                String imeZanra=cursor.getString(cursor.getColumnIndex("zanr_ime"));
                Zanr tmp=new Zanr(imeZanra);

                listaZanrova.add(tmp);


                cursor.moveToNext();
            }
        }

        Cursor cursor2=db.rawQuery("SELECT * FROM "+GlumacReziser_tabela+" WHERE glumac_id="+idGlumca,null);

        if(cursor2.moveToFirst())
        {
            while (!cursor2.isAfterLast())
            {
                String idRezisera=cursor2.getString(cursor2.getColumnIndex("reziser_id"));
                Cursor cursor3=db.rawQuery("SELECT * FROM reziseri"+" WHERE id="+idRezisera,null);
                if(cursor3.moveToFirst())
                {
                    while (!cursor3.isAfterLast())
                    {
                        String imeRezisera=cursor3.getString(cursor3.getColumnIndex(Reziser_ime));
                        Reziser tmpRez=new Reziser(imeRezisera,idRezisera);
                        listaRezisera.add(tmpRez);
                        cursor3.moveToNext();
                    }
                }

                cursor2.moveToNext();
            }
        }

        FragmentDetaljiGlumac.listaRezisera=listaRezisera;
        FragmentDetaljiGlumac.listaZanrova=listaZanrova;
    }

    public void UkloniGlumca(Glumac izabraniGlumac) {

        String glumacQuery = "DELETE FROM " + Glumac_tabela + " WHERE " + Glumac_gTheMoviedbID + "=" + izabraniGlumac.getgTheMoviedbID() + ";";
        db.execSQL(glumacQuery);
        String glumacReziserQuery = "DELETE FROM " + GlumacReziser_tabela + " WHERE " + RGlumac_id + "=" + izabraniGlumac.getgTheMoviedbID() + ";";
        db.execSQL(glumacReziserQuery);
        String glumacZanrQuery = "DELETE FROM " + GlumacZanr_tabela + " WHERE " + ZGlumac_id + "=" + izabraniGlumac.getgTheMoviedbID() + ";";
        db.execSQL(glumacZanrQuery);

        Cursor cursor = db.rawQuery("SELECT * FROM " + Reziser_tabela, null);

        if(cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                String idReziser = cursor.getString(cursor.getColumnIndex(Reziser_id));

                String query = "SELECT Count(*) FROM " + GlumacReziser_tabela + " WHERE " + idReziser + "=" +GReziser_id ;

                SQLiteStatement br=db.compileStatement(query);
                long brojRezisera=br.simpleQueryForLong();

                if(brojRezisera==0)
                db.execSQL("DELETE FROM " + Reziser_tabela + " WHERE "+Reziser_id+"="+idReziser);

                cursor.moveToNext();
            }
        }


        Cursor cursor2 = db.rawQuery("SELECT * FROM " + Zanr_tabela, null);

        if(cursor2.moveToFirst())
        {
            while(!cursor2.isAfterLast())
            {
                String imeZanra = cursor2.getString(cursor2.getColumnIndex(Zanr_ime));

                String query2 = "SELECT Count(*) FROM " + GlumacZanr_tabela + " WHERE \"" + imeZanra + "\"=" +GZanr_IME ;
                //String query2="SELECT Count(*) FROM glumciZanrovi WHERE zanr_ime=" +imeZanra ;
                SQLiteStatement br=db.compileStatement(query2);
                long brojZanrova=br.simpleQueryForLong();

                if(brojZanrova==0)
                db.execSQL("DELETE FROM " + Zanr_tabela + " WHERE \"" + imeZanra + "\"="+Zanr_ime);

                cursor2.moveToNext();
            }
        }

    }

    public void DajGlumceReziser(String unos) {

        Cursor cursor=db.rawQuery("SELECT * FROM "+Glumac_tabela+","+GlumacReziser_tabela+","+Reziser_tabela+" WHERE "+Glumac_gTheMoviedbID+"="+RGlumac_id+
                " AND "+GReziser_id+"="+Reziser_id,null);
//" AND \""+unos+"\"="+Reziser_ime
        if(cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                String imeRezisera=cursor.getString(cursor.getColumnIndex(Reziser_ime));
                if(imeRezisera.toLowerCase().contains(unos.toLowerCase()))
                {
                    String imeGlumca=cursor.getString(cursor.getColumnIndex("ime"));
                    String tmdbid=cursor.getString(cursor.getColumnIndex("tmdbid"));
                    String godinarodj=cursor.getString(cursor.getColumnIndex("godinaRodjenja"));
                    String godinasmrti=cursor.getString(cursor.getColumnIndex("godinaSmrti"));
                    String mjestorodj=cursor.getString(cursor.getColumnIndex("mjestoRodjenja"));
                    String rating=cursor.getString(cursor.getColumnIndex("rating"));
                    String bio=cursor.getString(cursor.getColumnIndex("biografija"));
                    String link=cursor.getString(cursor.getColumnIndex("link"));
                    String spol=cursor.getString(cursor.getColumnIndex("spol"));
                    byte[] slika=cursor.getBlob(cursor.getColumnIndex("slika"));
                    Bitmap sl= BitmapFactory.decodeByteArray(slika,0,slika.length);

                    Glumac gTmp=new Glumac(imeGlumca,godinarodj,godinasmrti,mjestorodj,rating,bio,link,spol);
                    gTmp.setSlikaGlumca(sl);
                    gTmp.setgTheMoviedbID(tmdbid);
                    boolean jelTrue=false;
                    for(int i=0;i<GlumacPomocna.getInstance().getGlumci().size();i++)
                    {

                        if(GlumacPomocna.getInstance().getGlumci().get(i).getgTheMoviedbID().equals(gTmp.getgTheMoviedbID()))
                        {
                            jelTrue=true;
                            break;
                        }

                    }
                    if(!jelTrue)
                    {
                        FragmentListaGlumaca.glumacAdapter.addGlumac(gTmp);
                        jelTrue=false;
                    }

                }



                cursor.moveToNext();
            }
        }

    }
}
