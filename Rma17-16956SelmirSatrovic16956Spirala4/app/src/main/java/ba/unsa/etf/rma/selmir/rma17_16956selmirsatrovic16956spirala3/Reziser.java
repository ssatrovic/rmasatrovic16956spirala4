package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Salva on 18-May-17.
 */

public class Reziser implements Parcelable {

    private String ime;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Reziser(){}
    public Reziser(String ime,String id) {
        this.ime = ime;
        this.id=id;
    }

    protected Reziser(Parcel in)
    {
        ime=in.readString();
        id=in.readString();
    }

    public static final Creator<Reziser> CREATOR = new Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel in) {
            return new Reziser(in);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }
    };

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ime);
        dest.writeString(id);
    }
}