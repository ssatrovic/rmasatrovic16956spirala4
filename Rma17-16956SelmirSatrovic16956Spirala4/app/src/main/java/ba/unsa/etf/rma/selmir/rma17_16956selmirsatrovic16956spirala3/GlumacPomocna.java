package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import java.util.ArrayList;

/**
 * Created by Salva on 18-May-17.
 */

public class GlumacPomocna {
    private static GlumacPomocna instanca=new GlumacPomocna();
    private ArrayList<Glumac> glumci;

    public static GlumacPomocna getInstance()
    {
        return instanca;
    }

    private GlumacPomocna()
    {
        glumci=new ArrayList<Glumac>();
    }

    public ArrayList<Glumac> getGlumci()
    {
        return glumci;
    }
}
